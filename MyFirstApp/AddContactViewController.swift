//
//  AddContactViewController.swift
//  MyFirstApp
//
//  Created by Sumit Bansal on 9/8/16.
//  Copyright © 2016 Sumit. All rights reserved.
//

import UIKit

class AddContactViewController: UIViewController,UITextFieldDelegate {

@IBOutlet weak var nameLabel: UILabel!
@IBOutlet weak var cell_noLabel: UILabel!
@IBOutlet weak var phoneLabel: UILabel!
@IBOutlet weak var emailLabel: UILabel!
    
    
@IBOutlet weak var nameTextField: UITextField!
@IBOutlet weak var cellTextField: UITextField!
@IBOutlet weak var phoneTextField: UITextField!
@IBOutlet weak var emailTextField: UITextField!

@IBOutlet weak var phonePreTextField: UITextField!
@IBOutlet weak var cellPreTextField: UITextField!


var newContact = [Contacts]()
var contactName : String = ""
var contactCell : String = ""
var contactPhone : String = ""
var contactEmail : String = ""
var flagForAdd = true
var duplicacyFlag = false

override func viewDidLoad()
{
    super.viewDidLoad()

    //Keyboard setting for number text fields
    cellTextField.keyboardType = UIKeyboardType.NumberPad
    phoneTextField.keyboardType = UIKeyboardType.NumberPad

    //Gradient fininsh Background
    let colorTop = UIColor(red: (82/255.0), green: (237/255.0), blue: (199/255.0), alpha: 1)
    let colorBottom = UIColor(red: (90/255.0), green: (200/255.0), blue: (251/255.0), alpha: 1)
    let gl: CAGradientLayer
    gl = CAGradientLayer()
    gl.colors = [ colorTop.CGColor, colorBottom.CGColor]
    gl.locations = [ 0.0, 1.0]
    gl.frame = self.view.bounds
    self.view.layer.insertSublayer(gl, atIndex: 0)
    
    //delegate for getting click in view
    let tapRecognizer = UITapGestureRecognizer()
    tapRecognizer.addTarget(self, action: #selector(AddContactViewController.didTapView))
    self.view.addGestureRecognizer(tapRecognizer)

}

override func didReceiveMemoryWarning() {
super.didReceiveMemoryWarning()
// Dispose of any resources that can be recreated.
}



@IBAction func validateValues(sender: AnyObject)
{
    flagForAdd = true

    contactName = nameTextField.text!
    contactCell = cellTextField.text!
    contactPhone = phoneTextField.text!
    contactEmail = emailTextField.text!

    if (cellTextField.text == "" || nameTextField.text == "")
    {
    
    let alert = UIAlertController(title: "Missing Fields", message: "Name and Cell fields are Mandatory", preferredStyle:UIAlertControllerStyle.Alert)
    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
    self.presentViewController(alert, animated: true, completion: nil)

        flagForAdd = false
    }
    
    if(cellTextField.text?.characters.count < 10){
        flagForAdd = false

        let alert = UIAlertController(title: "Improper Number", message: "Number should have 10 digits", preferredStyle:UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)

        
    }
    
    
    if (cellTextField.text == phoneTextField.text){
    
    let alert = UIAlertController(title: "Same Numbers", message: "Do not replicate the entry", preferredStyle: UIAlertControllerStyle.Alert)
    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
    self.presentViewController(alert, animated: true, completion: nil)
    
    flagForAdd = false
    
    }
    
    if (phoneTextField.text!.isEmpty){
        
        //Check if phone text field is empty here
        // NO Validation
        
    }
        
    else{
        if(phoneTextField.text?.characters.count < 10){
            flagForAdd = false

            let alert = UIAlertController(title: "Improper Number", message: "Number should have 10 digits", preferredStyle:UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
        
    }
    
    
    

    if (emailTextField.text!.isEmpty){
        
        //Check if email text field is empty here
        
    }

else{
        
        //if not empty, do the validation for email

    if(isValidEmail(emailTextField.text!) == false)
    {
            let alert = UIAlertController(title: "Invalid Email Address", message: "Enter a Valid Email Address", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
    
            flagForAdd = false
            
    }

    }
    
   // let check = ContactsList()

    duplicacyFlag = ContactsList.sharedInstanse.isDuplicate(cellTextField.text!)
    
    if duplicacyFlag{
        let alert = UIAlertController(title: "Replicate Entry", message: "Do not Enter Duplicate Cell No.", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        
        
    }
    
    
    if(flagForAdd == true && duplicacyFlag == false)
    {
    addToContact(contactName, cell: contactCell , phone: contactPhone, email: contactEmail )
    }
    
    
}


func isValidEmail(testStr:String) -> Bool {
    
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluateWithObject(testStr)
}


func addToContact(name : String, cell : String, phone : String, email : String){

    let newContact = (Contacts(name: name, cell_no: cell, phone_no: phone, email_id: email))
    ContactsList.sharedInstanse.addContactList(newContact)
    self.navigationController?.popViewControllerAnimated(true)


}

// MARK: UITextField Delegate Methods
func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {

if (textField == cellTextField || textField == phoneTextField)
    {
    //for length limit
    let prelength = textField.text!.characters.count
    let newlength = string.characters.count
    let length = prelength + newlength
    
    //for enter only number
    let inverseSet = NSCharacterSet(charactersInString:"0123456789").invertedSet
    let components = string.componentsSeparatedByCharactersInSet(inverseSet)
    let filtered = components.joinWithSeparator("")
    
    
        if (length>10 || string != filtered){
            return false
        }
        else{
            return true
        }
    }
return true
}


func didTapView(){
    
    self.view.endEditing(true)

    }


func textFieldShouldReturn(textField: UITextField) -> Bool {
if textField == nameTextField{
    nameTextField.resignFirstResponder()
    
}

if textField == cellTextField{
    cellTextField.resignFirstResponder()
    
}
if textField == phoneTextField{
    phoneTextField.resignFirstResponder()
}
if textField == emailTextField{
    emailTextField.resignFirstResponder()
}

       return true
}

/*
// MARK: - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
// Get the new view controller using segue.destinationViewController.
// Pass the selected object to the new view controller.
}
*/

}
