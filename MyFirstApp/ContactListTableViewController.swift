//
//  ContactListTableViewController.swift
//  MyFirstApp
//
//  Created by Sumit Bansal on 9/6/16.
//  Copyright © 2016 Sumit. All rights reserved.
//

import UIKit

class ContactListTableViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var contactList : [Contacts] = []
    var filteredContacts = [Contacts]()
    
    
    let searchController = UISearchController(searchResultsController: nil)
    
    @IBOutlet weak var contactTableView: UITableView!
    
    
    func filterContentforSearchText(searchText : String){
        filteredContacts = contactList.filter{ contacts in return contacts.name.lowercaseString.containsString(searchText.lowercaseString)
        }
        contactTableView.reloadData()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        contactTableView.tableHeaderView = searchController.searchBar
        
    }
    
    override func viewWillAppear(animated: Bool) {
        contactList = ContactsList.sharedInstanse.contactLists()
        self.contactTableView.separatorInset = UIEdgeInsetsZero
        self.contactTableView.reloadData()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

     func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(searchController.active && searchController.searchBar.text != ""){
            return filteredContacts.count
        }
        else{
        return contactList.count
        }
        
    }

    
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ContactReusable", forIndexPath: indexPath)
        
        let contactAdd : Contacts
        if(searchController.active && searchController.searchBar.text != ""){
            contactAdd = filteredContacts[indexPath.row]

        }else{
            contactAdd = contactList[indexPath.row]
        }
        
        
        cell.textLabel?.text = contactAdd.name
        cell.layoutMargins = UIEdgeInsetsZero

        let cell_no_inString:String=String(contactAdd.cell_no);
        cell.detailTextLabel?.text = cell_no_inString
        return cell
    }
    


    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?){
        
        if (segue.identifier == "showContactDetails") {
            
            // initialize new view controller and cast it as your view controller
            let viewController = segue.destinationViewController as! MyViewController
            // your new view controller should have property that will store passed value
            let indexPath = self.contactTableView.indexPathForSelectedRow!
            let contactAdd = contactList[indexPath.row]
            
            viewController.name = contactAdd.name
            viewController.cell = contactAdd.cell_no
            viewController.contactPhone = contactAdd.phone_no
            viewController.contactEmail = contactAdd.email_id
        }
        
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ContactListTableViewController : UISearchResultsUpdating{
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        filterContentforSearchText(searchController.searchBar.text!)
    }
    
}