//
//  File.swift
//  MyFirstApp
//
//  Created by Sumit Bansal on 9/6/16.
//  Copyright © 2016 Sumit. All rights reserved.
//

import Foundation
import UIKit


class Contacts{
    
    var name : String
    var cell_no : String
    var phone_no : String
    var email_id : String
    
    
    init(name: String,cell_no : String , phone_no : String , email_id : String){
        
        self.name = name
        self.cell_no = cell_no
        self.phone_no = phone_no
        self.email_id = email_id
    }
    
    
}