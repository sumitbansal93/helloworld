//
//  ContactsList.swift
//  MyFirstApp
//
//  Created by Sumit Bansal on 9/6/16.
//  Copyright © 2016 Sumit. All rights reserved.
//

import Foundation
import UIKit

class ContactsList {
    static let sharedInstanse = ContactsList()
    
    var contact : [Contacts] = []
    init(){
        self.preparedList()
    }
    
    func contactLists() -> [Contacts] {
        
        return contact
        
    }
    
    private func preparedList()
    {
        contact.append(Contacts(name: "Sumit",cell_no : "9868569966" , phone_no : "9871350451" , email_id : "sumitbansal93@gmail.com"))
        contact.append(Contacts(name: "Bansal",cell_no : "9871350451" , phone_no : "9868569966" , email_id : "sumit@tokbox.com"))
    }
    
    
    // MARK: Utility Methods
    
    func addContactList(newContact: Contacts) {
        contact.append(newContact);
    }

    func isDuplicate(cell_no : String) -> Bool{
        var flag = false
        
        for i in 0  ..< contact.count
            {
                if(contact[i].cell_no == cell_no){
                flag = true
                break
            }
        }
     return flag
    }

    
}