//
//  MyViewController.swift
//  MyFirstApp
//
//  Created by Sumit Bansal on 10/12/16.
//  Copyright © 2016 Sumit. All rights reserved.
//

import UIKit


class MyViewController: UIViewController {
    
    var name : String = ""
    var cell :String = ""
    var contactPhone : String = ""
    var contactEmail : String = ""
    
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var cellTextField: UITextField!
    @IBOutlet weak var phonePreTextField: UITextField!
    @IBOutlet weak var cellPreTextField: UITextField!
    
    
    override func viewWillAppear(animated: Bool) {
        nameTextField.text = name
        cellTextField.text = cell
        phoneTextField.text = contactPhone
        emailTextField.text = contactEmail
        
        //Gradient fininsh Background
        let colorTop = UIColor(red: (82/255.0), green: (237/255.0), blue: (199/255.0), alpha: 1)
        let colorBottom = UIColor(red: (90/255.0), green: (200/255.0), blue: (251/255.0), alpha: 1)
        
        let gradientLayer: CAGradientLayer
        gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ colorTop.CGColor, colorBottom.CGColor]
        gradientLayer.locations = [ 0.0, 1.0]
        gradientLayer.frame = self.view.bounds
       // self.view.backgroundColor = UIColor.brownColor()
     self.view.layer.insertSublayer(gradientLayer, atIndex: 0)
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
       
    }
    
}